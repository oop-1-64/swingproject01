/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.swingproject01.swingHello;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author domem
 */
public class Bot {
    private int handPlayer; //handPlayer: 0 = rock, 1 = scissors, 2 = paper
    private int hand;       //hand:       0 = rock, 1 = scissors, 2 = paper
    private int win,lose,draw;
    private int status;
    public Bot(){
    
    }
    public int randomAnswer(){
        return ThreadLocalRandom.current().nextInt(0,3);
    }
    public int result(int handPlayer){ //status: 0 = draw ,1 = win ,-1 = lose 
        this.handPlayer = handPlayer;
        this.hand = randomAnswer();
        if (handPlayer == hand){
            status = 0;
            return draw++;
        }
        if ((handPlayer == 0 && hand == 1) || (handPlayer == 1 && hand == 2) || (handPlayer == 2 && hand == 0)){
            status = 1;
            return win++;
        }
        status = -1;
        return lose++;
    }

    public int getHandPlayer() {
        return handPlayer;
    }

    public int getHand() {
        return hand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }
    
    
}
